// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.crackzip;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.DataFormatException;

import org.junit.jupiter.api.Test;
import org.refcodes.exception.Trap;
import org.refcodes.runtime.Execution;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.inputstream.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.LocalFileHeader;

public class TestCrackZip {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCrackZipFile1() throws IOException {
		File theSampleZip = new File( "src/test/resources/sample.zipcrypto.zip" );
		if ( Execution.isUnderTest() ) {
			System.out.println( theSampleZip.getAbsolutePath() );
		}
		String thePass = "Secret123!";
		try ( ZipFile theZipFile = new ZipFile( theSampleZip ) ) {
			theZipFile.setPassword( thePass.toCharArray() );
			List<FileHeader> theHeaders = theZipFile.getFileHeaders();
			for ( FileHeader eHeader : theHeaders ) {
				if ( Execution.isUnderTest() ) {
					System.out.println( eHeader.getFileName() );
				}
			}
		}
		if ( Execution.isUnderTest() ) {
			System.out.println();
		}
	}

	@Test
	public void testCrackZipStream1() throws IOException {
		File theSampleZip = new File( "src/test/resources/sample.zip" );
		if ( Execution.isUnderTest() ) {
			System.out.println( theSampleZip.getAbsolutePath() );
		}
		try ( ZipInputStream theZipStream = new ZipInputStream( new FileInputStream( theSampleZip ) ) ) {
			LocalFileHeader theFirstEntry = theZipStream.getNextEntry();
			if ( Execution.isUnderTest() ) {
				System.out.println( theFirstEntry.getFileName() );
				System.out.println( "Size = " + theFirstEntry.getUncompressedSize() );
				System.out.println( "Offset = " + theFirstEntry.getOffsetStartOfData() );
				byte[] theData = new byte[(int) theFirstEntry.getUncompressedSize()];
				theZipStream.read( theData );
				String theMessage = new String( theData );
				System.out.println( theMessage );
			}
		}
		if ( Execution.isUnderTest() ) {
			System.out.println();
		}
	}

	@Test
	public void testCrackZipStreamZipCrypto1() throws IOException {
		File theSampleZip = new File( "src/test/resources/sample.zipcrypto.zip" );
		if ( Execution.isUnderTest() ) {
			System.out.println( theSampleZip.getAbsolutePath() );
		}
		String thePass = "Secret123!";
		try ( ZipInputStream theZipStream = new ZipInputStream( new FileInputStream( theSampleZip ) ) ) {
			theZipStream.setPassword( thePass.toCharArray() );
			LocalFileHeader theFirstEntry = theZipStream.getNextEntry();
			if ( Execution.isUnderTest() ) {
				System.out.println( theFirstEntry.getFileName() );
				System.out.println( "Size = " + theFirstEntry.getUncompressedSize() );
				System.out.println( "Offset = " + theFirstEntry.getOffsetStartOfData() );
				byte[] theData = new byte[(int) theFirstEntry.getUncompressedSize()];
				theZipStream.read( theData );
				String theMessage = new String( theData );
				System.out.println( theMessage );
			}
		}
		if ( Execution.isUnderTest() ) {
			System.out.println();
		}
	}

	@Test
	public void testCrackZipStreamZipCrypto2() throws IOException {
		File theSampleZip = new File( "src/test/resources/sample.zipcrypto.zip" );
		System.out.println( theSampleZip.getAbsolutePath() );
		String thePass = "Secret123 ";
		try ( ZipInputStream theZipStream = new ZipInputStream( new FileInputStream( theSampleZip ) ) ) {
			theZipStream.setPassword( thePass.toCharArray() );
			LocalFileHeader theFirstEntry = theZipStream.getNextEntry();
			if ( Execution.isUnderTest() ) {
				System.out.println( theFirstEntry.getFileName() );
				System.out.println( "Size = " + theFirstEntry.getUncompressedSize() );
				System.out.println( "Offset = " + theFirstEntry.getOffsetStartOfData() );
				byte[] theData = new byte[(int) theFirstEntry.getUncompressedSize()];
				theZipStream.read( theData );
				String theMessage = new String( theData );
				System.out.println( theMessage );
			}
			fail( "Expecting a <" + ZipException.class.getName() + ">!" );
		}
		catch ( ZipException e ) {
			if ( Execution.isUnderTest() ) {
				System.out.println( Trap.asMessage( e ) );
			}
		}
		if ( Execution.isUnderTest() ) {
			System.out.println();
		}
	}

	@Test
	public void testCrackZipStreamZipCrypto3() throws IOException {
		File theSampleZip = new File( "src/test/resources/sample.zipcrypto.zip" );
		System.out.println( theSampleZip.getAbsolutePath() );
		String thePass = "Secret10FO";
		try ( ZipInputStream theZipStream = new ZipInputStream( new FileInputStream( theSampleZip ) ) ) {
			theZipStream.setPassword( thePass.toCharArray() );
			LocalFileHeader theFirstEntry = theZipStream.getNextEntry();
			if ( Execution.isUnderTest() ) {
				System.out.println( theFirstEntry.getFileName() );
				System.out.println( "Size = " + theFirstEntry.getUncompressedSize() );
				System.out.println( "Offset = " + theFirstEntry.getOffsetStartOfData() );
				System.out.println( "Crc = " + theFirstEntry.getCrc() );
				//	byte[] theData = new byte[(int) theFirstEntry.getUncompressedSize()];
				//	theZipStream.read( theData );
				//	String theMessage = new String( theData );
				//	System.out.println( theMessage );

				for ( int i = 0; i < theFirstEntry.getUncompressedSize(); i++ ) {
					theZipStream.read();
				}
			}
			fail( "Expecting a <" + ZipException.class.getName() + ">!" );
		}
		catch ( ZipException e ) {
			if ( Execution.isUnderTest() ) {
				System.out.println( Trap.asMessage( e ) );
			}
		}
		if ( Execution.isUnderTest() ) {
			System.out.println();
		}
	}

	@Test
	public void testCrackZipStreamAes1() throws IOException {
		File theSampleZip = new File( "src/test/resources/sample.aes.zip" );
		if ( Execution.isUnderTest() ) {
			System.out.println( theSampleZip.getAbsolutePath() );
		}
		String thePass = "Secret123!";
		try ( ZipInputStream theZipStream = new ZipInputStream( new FileInputStream( theSampleZip ) ) ) {
			theZipStream.setPassword( thePass.toCharArray() );
			LocalFileHeader theFirstEntry = theZipStream.getNextEntry();
			if ( Execution.isUnderTest() ) {
				System.out.println( theFirstEntry.getFileName() );
				System.out.println( "Size = " + theFirstEntry.getUncompressedSize() );
				System.out.println( "Offset = " + theFirstEntry.getOffsetStartOfData() );
				byte[] theData = new byte[(int) theFirstEntry.getUncompressedSize()];
				theZipStream.read( theData );
				String theMessage = new String( theData );
				System.out.println( theMessage );
			}
		}
		if ( Execution.isUnderTest() ) {
			System.out.println();
		}
	}

	@Test
	public void testCrackZipStreamAes2() throws IOException {
		File theSampleZip = new File( "src/test/resources/sample.aes.zip" );
		System.out.println( theSampleZip.getAbsolutePath() );
		String thePass = "Secret123 ";
		try ( ZipInputStream theZipStream = new ZipInputStream( new FileInputStream( theSampleZip ) ) ) {
			theZipStream.setPassword( thePass.toCharArray() );
			LocalFileHeader theFirstEntry = theZipStream.getNextEntry();
			if ( Execution.isUnderTest() ) {
				System.out.println( theFirstEntry.getFileName() );
				System.out.println( "Size = " + theFirstEntry.getUncompressedSize() );
				System.out.println( "Offset = " + theFirstEntry.getOffsetStartOfData() );
				byte[] theData = new byte[(int) theFirstEntry.getUncompressedSize()];
				theZipStream.read( theData );
				String theMessage = new String( theData );
				System.out.println( theMessage );
			}
			fail( "Expecting a <" + ZipException.class.getName() + ">!" );
		}
		catch ( ZipException e ) {
			if ( Execution.isUnderTest() ) {
				System.out.println( Trap.asMessage( e ) );
			}
		}
		if ( Execution.isUnderTest() ) {
			System.out.println();
		}
	}

	@Test
	public void testCrackZipStreamAes3() throws IOException {
		File theSampleZip = new File( "src/test/resources/sample.aes.zip" );
		System.out.println( theSampleZip.getAbsolutePath() );
		String thePass = "Secre00<g";
		try ( ZipInputStream theZipStream = new ZipInputStream( new FileInputStream( theSampleZip ) ) ) {
			theZipStream.setPassword( thePass.toCharArray() );
			LocalFileHeader theFirstEntry = theZipStream.getNextEntry();
			if ( Execution.isUnderTest() ) {
				System.out.println( theFirstEntry.getFileName() );
				System.out.println( "Size = " + theFirstEntry.getUncompressedSize() );
				System.out.println( "Offset = " + theFirstEntry.getOffsetStartOfData() );
				//	byte[] theData = new byte[(int) theFirstEntry.getUncompressedSize()];
				//	theZipStream.read( theData );
				//	String theMessage = new String( theData );
				//	System.out.println( theMessage );

				byte[] theBuffer = new byte[theFirstEntry.getUncompressedSize() < 1024 ? (int) theFirstEntry.getUncompressedSize() : 1024];
				theZipStream.read( theBuffer );
			}
			fail( "Expecting a <" + IOException.class.getName() + ">!" );
		}
		catch ( IOException e ) {
			if ( Execution.isUnderTest() ) {
				System.out.println( Trap.asMessage( e ) );
			}
			assertTrue( e.getCause() instanceof DataFormatException );
		}

		if ( Execution.isUnderTest() ) {
			System.out.println();
		}
	}
}
