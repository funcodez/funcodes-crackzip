// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.crackzip;

import static org.refcodes.cli.CliSugar.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ArrayOption;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.component.LifecycleException;
import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.CharSet;
import org.refcodes.data.ExitCode;
import org.refcodes.exception.Trap;
import org.refcodes.generator.AlphabetCounterComposite;
import org.refcodes.generator.AlphabetCounterMetrics;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.Properties;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.properties.TomlPropertiesBuilder;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.Terminal;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.FileHeader;
import sun.misc.Signal;

/**
 * Tries to recover a ZIP (*-zip) archive's password.
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "crackzip";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String TITLE = "…CRACKX7IP…";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/crackzip_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "Recovery tool for cracking password protected ZIP (*.zip) files (see [https://www.metacodes.pro/manpages/crackzip_manpage]).";
	private static final String ZIPFILE_PROPERTY = "zipFile";
	private static final String WORDLIST_PROPERTY = "wordFile";
	private static final String OUTPUTDIR_PROPERTY = "outputDir";
	private static final String INFO_PROPERTY = "info";
	private static final String BRUTEFORCE_PROPERTY = "bruteForce";
	private static final String INIT_PROPERTY = "init";
	private static final String CHARSET_PROPERTY = "charSet";
	private static final String ALPHABET_PROPERTY = "alphabet";
	private static final String MINLENGTH_PROPERTY = "minLength";
	private static final String MAXLENGTH_PROPERTY = "maxLength";
	private static final String RESUMEFROM_PROPERTY = "resumeFrom";
	private static final String THREADS_PROPERTY = "threads";
	private static final String PRIORITY_PROPERTY = "priority";
	private static final String PASSWORD_PROPERTY = "password";
	private static final String NOCACHE_PROPERTY = "noCache";
	private static final String UPDATE_PROPERTY = "updateTimeMillis";
	private static final String TIMEELAPSED_PROPERTY = "timeElapsedSecs";
	private static final String PASSWORDCOUNT_PROPERTY = "passwordCount";
	private static final String EXPRESSION_PROPERTY = "expression";
	private static final String YES_REGEX_PROPERTY = "yesRegex";
	private static final String NO_REGEX_PROPERTY = "noRegex";
	private static final int DEFAULT_MINLENGTH = 1;
	private static final int DEFAULT_MAXLENGTH = -1;
	private static final CharSet DEFAULT_CHARSET = CharSet.ASCII;
	private static final int DEFAULT_UPDATE_TIME_MS = 2500;
	private static final String ESC_BOLD = Terminal.isAnsiTerminalEnabled() ? AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.BOLD ) : "";
	private static final String ESC_ATTENTION = Terminal.isAnsiTerminalEnabled() ? AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_YELLOW, AnsiEscapeCode.BG_BLUE ) : "";
	private static final String ESC_RESET = Terminal.isAnsiTerminalEnabled() ? AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.RESET ) : "";
	// private static final long DEFAULT_STDIN_TIMEOUT_MILLIS = 10000;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final EnumOption<CharSet> theCharSetArg = enumOption( "char-set", CharSet.class, CHARSET_PROPERTY, "The character set to use when probing passwords: " + VerboseTextBuilder.asString( CharSet.values() ) );
		final StringOption theAlphabetArg = stringOption( 'a', "alphabet", ALPHABET_PROPERTY, "The alphabet to when probing passwords." );
		final StringOption theWordListFileArg = stringOption( 'w', "word-list", WORDLIST_PROPERTY, "The word list file (one word per line) which contains the passwords to probe." );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", ZIPFILE_PROPERTY, "The input ZIP (*.zip) file which to examine." );
		final StringOption theOutputDirArg = stringOption( 'o', "output-dir", OUTPUTDIR_PROPERTY, "The output directory where to write the results to." );
		final StringOption theResumeArg = stringOption( 'r', "resume-from", RESUMEFROM_PROPERTY, "Resume the operation from the given password." );
		final StringOption theExpressionArg = stringOption( 'e', "expression", EXPRESSION_PROPERTY, "The expression(s) in combination defining the password each as of \"[minLength-maxLength]:{char(s),char(s),...,char(s)}:'startValue'\". e.g. \"[3-12]:{A-Z,0-9,ASCII_SPECIAL,U+03B1,U+03B2,U+03B3,a,b,c}:'a10!Y'\" (probed password's min length is 3, max length is 12, valid characters include all upper case letters from 'A' to 'Z', all digits from '0' to '9', the ASCII_SPECIAL char(s), the letters 'alpha', 'beta', 'gamma' as of the given UTF codepoints as well as the letters 'a', 'b', 'c' and the section's start value \"a10!Y\"). Predefined char sets as of: " + VerboseTextBuilder.asString( CharSet.values() ) );

		final StringOption theYesExpArg = stringOption( 'y', "yes-regex", YES_REGEX_PROPERTY, "A regular expression which must match a password to be included(!) for probing, e.g. \"\\b.*?(.)\\1.*?\\b\" to include only those password with at least two same characters directly next to each other." );
		final StringOption theNoExpArg = stringOption( 'n', "no-regex", NO_REGEX_PROPERTY, "A regular expression which must match a password to be skipped(!) for probing, e.g. \"\\b.*?(.)\\1.*?\\b\" to skip those password with at least two same characters directly next to each other." );

		final ArrayOption<String> theExpressionArgs = asArray( theExpressionArg );
		final Flag theInfoFlag = flag( "info", INFO_PROPERTY, "Show detailed information about the ZIP (*.zip) input  file." );
		final Flag theBruteForceFlag = flag( 'b', "brute-force", BRUTEFORCE_PROPERTY, "Start brute force password recovery." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theInitFlag = flag( "init", INIT_PROPERTY, "Initialize a new project." );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final Flag theCleanFlag = cleanFlag( false );
		final Flag theNoCacheFlag = flag( "no-cache", NOCACHE_PROPERTY, "Experimental, do not(!) load ZIP (*.zip) input file into memory (no speedup) before processing." );
		final IntOption theMinLengthArg = intOption( "min-length", MINLENGTH_PROPERTY, "Min password length to probe (defaults to <" + DEFAULT_MINLENGTH + ">)." );
		final IntOption theMaxLengthArg = intOption( "max-length", MAXLENGTH_PROPERTY, "Max password length to probe, a value of -1 denotes an infinite max length (defaults to <" + DEFAULT_MAXLENGTH + ">)." );
		final IntOption theThreadsArg = intOption( 't', "threads", THREADS_PROPERTY, "Number of threads to use (defaults to <" + Runtime.getRuntime().availableProcessors() + "> on this machine)." );
		final IntOption thePriorityArg = intOption( 'p', "priority", PRIORITY_PROPERTY, "Thread priority (min = <" + Thread.MIN_PRIORITY + ">, max = <" + Thread.MAX_PRIORITY + ">) to use (defaults to <" + Thread.NORM_PRIORITY + ">)." );
		final IntOption theUpdateTimeArg = intOption( 'u', "update-time", UPDATE_PROPERTY, "Update time in ms of update current execution status into project file and, upon verbose mode, on the screen (defaults to <" + DEFAULT_UPDATE_TIME_MS + ">)." );

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( theBruteForceFlag, theInputFileArg, any( theOutputDirArg, xor( theWordListFileArg, theExpressionArgs,  any( theMinLengthArg, theMaxLengthArg, xor( theCharSetArg, theAlphabetArg), theResumeArg ) ), theYesExpArg, theNoExpArg, theThreadsArg, thePriorityArg, theUpdateTimeArg, theNoCacheFlag, theVerboseFlag, theDebugFlag ) ), 
			and( theInitFlag, theInputFileArg, any( theVerboseFlag, theDebugFlag ) ),
			and( theCleanFlag, theInputFileArg, any( theVerboseFlag, theDebugFlag ) ),
			and( theInfoFlag, theInputFileArg, any( theVerboseFlag, theDebugFlag ) ),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Crack archive password using a char set", theBruteForceFlag, theInputFileArg, theCharSetArg, theVerboseFlag),
			example( "Crack archive password using a custom alphabet", theBruteForceFlag, theInputFileArg,theAlphabetArg, theVerboseFlag),
			example( "Crack archive password using a word list", theBruteForceFlag, theInputFileArg,theWordListFileArg, theVerboseFlag),
			example( "Crack archive starting at password using a char set", theBruteForceFlag, theInputFileArg, theCharSetArg, theResumeArg, theVerboseFlag),
			example( "Crack archive starting at password using a custom alphabet", theBruteForceFlag, theInputFileArg, theAlphabetArg, theResumeArg, theVerboseFlag),
			example( "Crack archive starting at password using a a word list", theBruteForceFlag, theInputFileArg, theWordListFileArg, theResumeArg, theVerboseFlag),
			example( "Crack archive with passwords [min..max] using a char set", theBruteForceFlag, theInputFileArg, theCharSetArg, theMinLengthArg, theMaxLengthArg, theVerboseFlag),
			example( "Crack archive with passwords [min..max] using a custom alphabet", theBruteForceFlag, theInputFileArg, theAlphabetArg, theMinLengthArg, theMaxLengthArg, theVerboseFlag),
			example( "Crack archive with number of threads and thread priorities", theBruteForceFlag, theInputFileArg, theThreadsArg, thePriorityArg, theVerboseFlag),
			example( "Crack archive with three expressions for complex passwords to probe", theBruteForceFlag, theInputFileArg, theExpressionArg, theExpressionArg, theExpressionArg),
			example( "Crack archive and extract cracked content to output folder", theBruteForceFlag, theInputFileArg, theOutputDirArg, theVerboseFlag),
			example( "Update frequency (ms) of current execution status into project file", theBruteForceFlag, theInputFileArg, theUpdateTimeArg, theVerboseFlag),
			example( "Clean archive's project configuration", theCleanFlag, theInputFileArg, theVerboseFlag),
			example( "Show ZIP file information", theInfoFlag, theInputFileArg, theVerboseFlag),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();
		final boolean isDebug = theArgsProperties.getBoolean( theDebugFlag );

		try {
			final String theZipFilePath = theInputFileArg.getValue();
			String theProjectFilePath = theZipFilePath + ".ini";
			if ( !theProjectFilePath.endsWith( ".ini" ) ) {
				theProjectFilePath = theProjectFilePath + ".ini";
			}

			// -----------------------------------------------------------------
			// Info:
			// -----------------------------------------------------------------
			if ( theInfoFlag.isEnabled() ) {
				info( theZipFilePath, isVerbose );
			}
			// -----------------------------------------------------------------
			// Crack:
			// -----------------------------------------------------------------
			else if ( theBruteForceFlag.isEnabled() ) {

				// -------------------------------------------------------------
				// ZIP file:
				// -------------------------------------------------------------

				final File theZipFile = toZipFile( theZipFilePath, isVerbose );

				// -------------------------------------------------------------
				// Project properties:
				// -------------------------------------------------------------

				if ( isVerbose ) {
					LOGGER.printSeparator();
				}
				final File theProjectFile = toProjectFile( theProjectFilePath, isVerbose );
				if ( theProjectFile.isDirectory() ) {
					throw new IOException( "Project file \"" + theProjectFilePath + "\" (<" + theProjectFile.getAbsolutePath() + ">) points to a directory!" );
				}
				if ( !theProjectFile.exists() ) {
					theCliHelper.createResourceFile( DEFAULT_CONFIG, theProjectFile );
				}
				final ResourcePropertiesBuilder theProjectProperties = new TomlPropertiesBuilder( theProjectFile );
				theProjectProperties.putAll( theArgsProperties.getArgsParserProperties() );

				if ( theAlphabetArg.hasValue() || theCharSetArg.hasValue() ) {
					theProjectProperties.remove( ALPHABET_PROPERTY );
					theProjectProperties.remove( CHARSET_PROPERTY );
				}

				// -------------------------------------------------------------
				// Output dir:
				// -------------------------------------------------------------

				if ( isVerbose ) {
					LOGGER.printSeparator();
				}
				final String theOutputDirPath = theProjectProperties.get( OUTPUTDIR_PROPERTY );
				File theOutputDir = null;
				if ( theOutputDirPath != null && theOutputDirPath.length() != 0 ) {
					theOutputDir = new File( theOutputDirPath );
					if ( isVerbose ) {
						LOGGER.info( "Output dir = \"" + theOutputDirPath + "\" (<" + theOutputDir.getAbsolutePath() + ">)" );
					}
					if ( !theOutputDir.isDirectory() ) {
						if ( !theOutputDir.mkdir() ) {
							throw new IOException( "Cannot create output dir \"" + theOutputDirPath + "\" (<" + theOutputDir.getAbsolutePath() + ">)!" );
						}
					}
				}

				// -------------------------------------------------------------
				// Regex
				// -------------------------------------------------------------

				final String theYesRegex = theProjectProperties.get( YES_REGEX_PROPERTY );
				final String theNoRegex = theProjectProperties.get( NO_REGEX_PROPERTY );
				final Pattern theYesPattern = theYesRegex != null && theYesRegex.length() != 0 ? Pattern.compile( theYesRegex ) : null;
				final Pattern theNoPattern = theNoRegex != null && theNoRegex.length() != 0 ? Pattern.compile( theNoRegex ) : null;
				if ( isVerbose ) {
					if ( theYesPattern != null ) {
						LOGGER.info( "Yes regex = \"" + theYesRegex + "\" (<" + theYesPattern.pattern() + ">)" );
					}
					if ( theNoPattern != null ) {
						LOGGER.info( "No regex = \"" + theNoRegex + "\" (<" + theNoPattern.pattern() + ">)" );
					}
				}

				// -------------------------------------------------------------
				// Alphabet Counter:
				// -------------------------------------------------------------

				final boolean hasAlphabetArg = theMinLengthArg.hasValue() || theMaxLengthArg.hasValue() || theAlphabetArg.hasValue() || theCharSetArg.hasValue() || theResumeArg.hasValue();
				final boolean hasExpressionArg = theExpressionArgs.hasValue();
				final boolean hasWordListFileArg = theWordListFileArg.hasValue();
				final AlphabetCounterMetrics[] theAlphabetCounterMetrics;
				if ( hasAlphabetArg ) {
					cleanProjectExpression( theProjectProperties, isVerbose );
					int theMinLength = theProjectProperties.getIntOr( MINLENGTH_PROPERTY, DEFAULT_MINLENGTH );
					int theMaxLength = theProjectProperties.getIntOr( MAXLENGTH_PROPERTY, DEFAULT_MAXLENGTH );
					String theResume = theProjectProperties.get( RESUMEFROM_PROPERTY );
					String theAlphabet = theProjectProperties.get( ALPHABET_PROPERTY );
					CharSet theCharSet = theProjectProperties.getEnum( CharSet.class, CHARSET_PROPERTY );
					theAlphabetCounterMetrics = new AlphabetCounterMetrics[] { toAlphabetCounterMetrics( theResume, theMinLength, theMaxLength, theAlphabet, theCharSet ) };
					resetProjectProperties( theProjectProperties );
				}
				else if ( hasWordListFileArg ) {
					cleanProjectExpression( theProjectProperties, isVerbose );
					String theResume = theProjectProperties.get( RESUMEFROM_PROPERTY );
					String theWordListFilePath = theWordListFileArg.getValue();
					File theWordListFile = new File( theWordListFilePath );
					if ( isVerbose ) {
						LOGGER.printSeparator();
						LOGGER.info( "Word list = " + theWordListFile.getAbsolutePath() );
					}
					theAlphabetCounterMetrics = new AlphabetCounterMetrics[] { new AlphabetCounterMetrics( theResume, theWordListFile ) };
					resetProjectProperties( theProjectProperties );
				}
				else if ( hasExpressionArg ) {
					resetProjectProperties( theProjectProperties );
					if ( theExpressionArgs.hasValue() ) {
						theProjectProperties.putArray( EXPRESSION_PROPERTY, theExpressionArgs.getValue() );
					}
					String[] theExpressions = theProjectProperties.getArray( EXPRESSION_PROPERTY );
					if ( theExpressions == null || theExpressions.length == 0 ) {
						throw new IllegalArgumentException( "You provided empty expression(s) " + ( theExpressions == null ? "<null>" : "<{}>" ) + " for argument (property) <" + EXPRESSION_PROPERTY + "> though at least one expression is required!" );
					}
					theAlphabetCounterMetrics = new AlphabetCounterMetrics[theExpressions.length];
					for ( int i = 0; i < theExpressions.length; i++ ) {
						theAlphabetCounterMetrics[i] = new AlphabetCounterMetrics( theExpressions[i] );
					}
				}
				else {
					String[] theExpressions = theProjectProperties.getArray( RESUMEFROM_PROPERTY );
					if ( theExpressions == null || theExpressions.length == 0 ) {
						theExpressions = theProjectProperties.getArray( EXPRESSION_PROPERTY );
					}
					if ( theExpressions == null || theExpressions.length == 0 ) {
						throw new IllegalArgumentException( "You provided empty expression(s) " + ( theExpressions == null ? "<null>" : "<{}>" ) + " for argument (property) <" + EXPRESSION_PROPERTY + "> though at least one expression is required!" );
					}
					theAlphabetCounterMetrics = new AlphabetCounterMetrics[theExpressions.length];
					for ( int i = 0; i < theExpressions.length; i++ ) {
						theAlphabetCounterMetrics[i] = new AlphabetCounterMetrics( theExpressions[i] );
					}
				}

				if ( isVerbose ) {
					LOGGER.printSeparator();
					LOGGER.info( "Number of expressions being processed = " + theAlphabetCounterMetrics.length );
					for ( AlphabetCounterMetrics eMetrics : theAlphabetCounterMetrics ) {
						LOGGER.printSeparator();
						LOGGER.info( "Expression = " + eMetrics.toAlphabetExpression() );
						LOGGER.info( "Min length = " + eMetrics.getMinLength() );
						LOGGER.info( "Max length = " + ( eMetrics.getMaxLength() == -1 ? "∞" : eMetrics.getMaxLength() ) );
						if ( eMetrics.getAlphabet() != null ) {
							LOGGER.info( "Alphabet = " + VerboseTextBuilder.asString( eMetrics.getAlphabet() ) );
						}
						if ( eMetrics.getStartValue() != null ) {
							LOGGER.info( "Start value = " + eMetrics.getStartValue() );
						}
						if ( eMetrics.getWords() != null ) {
							LOGGER.info( "Words = " + VerboseTextBuilder.asString( eMetrics.getWords() ) );
						}
					}
				}

				final AlphabetCounterComposite theAlphabetCounter = new AlphabetCounterComposite( theAlphabetCounterMetrics );

				// -------------------------------------------------------------
				// Threads and priority:
				// -------------------------------------------------------------

				final int theThreads = theProjectProperties.getIntOr( THREADS_PROPERTY, Runtime.getRuntime().availableProcessors() / 2 + 1 );
				if ( isVerbose ) {
					LOGGER.printSeparator();
					LOGGER.info( "Number of threads to use: " + theThreads );
				}
				final int thePriority = theProjectProperties.getIntOr( PRIORITY_PROPERTY, Thread.NORM_PRIORITY );
				if ( thePriority < Thread.MIN_PRIORITY || thePriority > Thread.MAX_PRIORITY ) {
					throw new IllegalArgumentException( "The thread priority <" + thePriority + "> must be between <" + Thread.MIN_PRIORITY + "> and <" + Thread.MAX_PRIORITY + ">!" );
				}
				if ( isVerbose ) {
					LOGGER.info( "Thread priority used: " + thePriority );
				}

				// -------------------------------------------------------------
				// In-memory processing:
				// -------------------------------------------------------------

				final boolean isNoCache = theProjectProperties.getBoolean( NOCACHE_PROPERTY );
				if ( isVerbose ) {
					LOGGER.info( "Process in-memory: " + ( isNoCache ? "No" : "Yes" ) );
				}

				// -------------------------------------------------------------
				// Update (refresh) time:
				// -------------------------------------------------------------
				final int theUpdateTime = theProjectProperties.getIntOr( UPDATE_PROPERTY, DEFAULT_UPDATE_TIME_MS );
				if ( isVerbose ) {
					LOGGER.info( "Update time (ms): " + theUpdateTime );
				}

				// -------------------------------------------------------------
				// Save properties:
				// -------------------------------------------------------------
				theProjectProperties.flush();

				// -------------------------------------------------------------
				// Start cracking:
				// -------------------------------------------------------------

				crackZipFile( theZipFile, theOutputDir, theAlphabetCounter, theYesPattern, theNoPattern, theThreads, thePriority, isNoCache, theUpdateTime, theProjectProperties, theResumeArg.getLongOption(), isVerbose, isDebug );
			}

			// -----------------------------------------------------------------
			// Init:
			// -----------------------------------------------------------------

			else if ( theInitFlag.isEnabled() ) {
				final File theProjectFile = toProjectFile( theProjectFilePath, isVerbose );
				theCliHelper.createResourceFile( DEFAULT_CONFIG, theProjectFile );
			}
			// Init:
			else if ( theCleanFlag.isEnabled() ) {
				init( theProjectFilePath, isVerbose );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static void cleanProjectExpression( ResourcePropertiesBuilder aProjectProperties, boolean isVerbose ) {
		Properties theRemoved = aProjectProperties.removeAll( EXPRESSION_PROPERTY, "**" );
		if ( theRemoved != null && theRemoved.size() != 0 ) {
			if ( isVerbose ) {
				LOGGER.printSeparator();
				for ( String eKey : theRemoved.sortedKeys() ) {
					LOGGER.info( "Discarding expression: \"" + theRemoved.get( eKey ) + "\" (overwritten by command line args)..." );
				}
			}
		}
	}

	private static void resetProjectProperties( ResourcePropertiesBuilder aProjectProperties ) {
		aProjectProperties.remove( MINLENGTH_PROPERTY );
		aProjectProperties.remove( MAXLENGTH_PROPERTY );
		aProjectProperties.remove( RESUMEFROM_PROPERTY );
		aProjectProperties.remove( ALPHABET_PROPERTY );
		aProjectProperties.remove( CHARSET_PROPERTY );
		aProjectProperties.remove( PASSWORDCOUNT_PROPERTY );
		aProjectProperties.remove( TIMEELAPSED_PROPERTY );
		aProjectProperties.remove( EXPRESSION_PROPERTY );
		aProjectProperties.removeAll( EXPRESSION_PROPERTY, "**" );
	}

	private static void crackZipFile( File aInFile, File aOutputDir, AlphabetCounterComposite aAlphabetCounterComposite, Pattern aYesPattern, Pattern aNoPattern, int aThreads, int aPriority, boolean isNoCache, long aUpdateResumeLoopTimeMs, ResourcePropertiesBuilder aProperties, String aResumeArg, boolean isVerbose, boolean isDebug ) throws IOException, LifecycleException {
		// ---------------------------------------------------------------------
		// Start threads:
		// ---------------------------------------------------------------------
		String theFirstPassword = aAlphabetCounterComposite.toString();
		long eTimeElapsedSecs = aProperties.getLongOr( TIMEELAPSED_PROPERTY, 0L );
		long thePasswordCount = aProperties.getLongOr( PASSWORDCOUNT_PROPERTY, 0L );
		long startTime = eTimeElapsedSecs == 0 ? System.currentTimeMillis() : System.currentTimeMillis() - ( eTimeElapsedSecs * 1000 );
		Thread.currentThread().setPriority( aPriority ); // At least the priority of the other threads to on going
		CrackZip theCrackZip = new CrackZip( aInFile, aOutputDir, aAlphabetCounterComposite, aYesPattern, aNoPattern, aThreads, aPriority, isNoCache );
		Signal.handle( new Signal( org.refcodes.runtime.Execution.CTRL_C_SIGNAL ), signal -> {
			try {
				theCrackZip.stopUnchecked();
			}
			catch ( Exception ignore ) {}
		} );
		// ---------------------------------------------------------------------
		// Crack loop:
		// ---------------------------------------------------------------------
		long ePasswordCount = 0;
		long ePasswordsPerSecond;
		if ( isVerbose ) {
			LOGGER.printSeparator();
		}
		String eLastProcessed = theFirstPassword;
		while ( !theCrackZip.isStopped() ) {
			aProperties.putArray( RESUMEFROM_PROPERTY, aAlphabetCounterComposite.toAlphabetExpressions() );
			eTimeElapsedSecs = ( System.currentTimeMillis() - startTime ) / 1000;
			ePasswordCount = thePasswordCount + theCrackZip.getProbedPasswordsCount();
			ePasswordsPerSecond = eTimeElapsedSecs != 0 ? ( ePasswordCount / eTimeElapsedSecs ) : -1;
			if ( isVerbose ) {
				LOGGER.trace( "Current probed password (of a total of <" + ePasswordCount + ">) is \"" + eLastProcessed + "\" (<" + ( ePasswordsPerSecond != -1 ? ePasswordsPerSecond : "?" ) + "> passwords/sec)..." );
			}
			synchronized ( theCrackZip ) {
				try {
					theCrackZip.wait( aUpdateResumeLoopTimeMs );
				}
				catch ( InterruptedException ignore ) {}
			}
			eLastProcessed = theCrackZip.lastProcessed();
			aProperties.flush();
		}
		ePasswordCount = thePasswordCount + theCrackZip.getProbedPasswordsCount();
		if ( isVerbose ) {
			long theThroughput = eTimeElapsedSecs != 0 ? ( ePasswordCount / eTimeElapsedSecs ) : -1;
			LOGGER.printSeparator();
			LOGGER.info( "Total time elapsed = <" + ( eTimeElapsedSecs != -1 ? eTimeElapsedSecs : "?" ) + "> seconds" );
			LOGGER.info( "Total number of passwords probed = <" + ePasswordCount + ">" );
			LOGGER.info( "Throughput = <" + theThroughput + "> passwords/sec" );
			LOGGER.printSeparator();
			if ( theCrackZip.getException() != null ) {
				if ( isDebug ) {
					LOGGER.warn( Trap.asMessage( theCrackZip.getException() ), theCrackZip.getException() );
				}
				else {
					LOGGER.warn( Trap.asMessage( theCrackZip.getException() ) );
				}
			}
		}
		String theResumeVerbose = aAlphabetCounterComposite.toString();
		aProperties.putLong( TIMEELAPSED_PROPERTY, eTimeElapsedSecs );
		aProperties.putLong( PASSWORDCOUNT_PROPERTY, ePasswordCount );
		// ---------------------------------------------------------------------
		// Password found?
		// ---------------------------------------------------------------------
		if ( theCrackZip.getPassword() != null ) {
			aProperties.putArray( RESUMEFROM_PROPERTY, aAlphabetCounterComposite.toAlphabetExpressions() );
			aProperties.put( PASSWORD_PROPERTY, theCrackZip.getPassword() );
			aProperties.flush();
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "Congratulations! Found your password in <" + ( ( System.currentTimeMillis() - startTime ) / 1000 ) + "> seconds!" );
				LOGGER.info( "Your password might be \"" + ESC_ATTENTION + theCrackZip.getPassword() + ESC_RESET + "\" (excluding the surrounding quotes)!" );
				if ( aOutputDir != null ) {
					LOGGER.info( "ZIP (*.zip) content written to: <" + aOutputDir.getAbsolutePath() + ">" );
				}
				LOGGER.printSeparator();
				LOGGER.info( "If this is not the case, recovery will resume at password \"" + ESC_BOLD + theResumeVerbose + ESC_RESET + "\"..." );
				LOGGER.printSeparator();
				LOGGER.info( "Don't forget to mail me a success story to <steiner@refcodes.org>! :-)" );
			}
			else {
				System.out.println( "Your password might be \"" + ESC_ATTENTION + theCrackZip.getPassword() + ESC_RESET + "\" (excluding the surrounding quotes), found in <" + ( ( System.currentTimeMillis() - startTime ) / 1000 ) + "> seconds!" );
				if ( aOutputDir != null ) {
					System.out.println( "ZIP (*.zip) content written to: <" + aOutputDir.getAbsolutePath() + ">" );
				}
				System.out.println( "If this is not the case, recovery will resume at password \"" + ESC_BOLD + theResumeVerbose + ESC_RESET + "\"..." );
				System.out.println( "Don't forget to mail me a success story to <steiner@refcodes.org>! :-)" );
			}
		}
		// ---------------------------------------------------------------------
		// Resume required?
		// ---------------------------------------------------------------------
		else if ( aAlphabetCounterComposite.hasNext() ) {
			aProperties.putArray( RESUMEFROM_PROPERTY, aAlphabetCounterComposite.toAlphabetExpressions() );
			aProperties.flush();
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "No success yet, last probed password number <" + ePasswordCount + "> is \"" + ESC_BOLD + theResumeVerbose + ESC_RESET + "\" (excluding the surrounding quotes)..." );
				LOGGER.printSeparator();
				LOGGER.info( "Recovery will resume at password \"" + theResumeVerbose + "\"..." );
			}
			else {
				System.out.println( "No success yet, last probed password number <" + ePasswordCount + "> is \"" + ESC_BOLD + theResumeVerbose + ESC_RESET + "\" (excluding the surrounding quotes)..." );
				System.out.println( "Recovery will resume at password \"" + theResumeVerbose + "\"..." );
			}
		}
		// ---------------------------------------------------------------------
		// Resume required?
		// ---------------------------------------------------------------------
		else {
			aProperties.putArray( RESUMEFROM_PROPERTY, aAlphabetCounterComposite.toAlphabetExpressions() );
			aProperties.flush();
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "No success after probing all passwords, last probed password number <" + ePasswordCount + "> was \"" + ESC_BOLD + theResumeVerbose + ESC_RESET + "\" (excluding the surrounding quotes)..." );
			}
			else {
				System.out.println( "No success after probing all passwords, last probed password number <" + ePasswordCount + "> was \"" + ESC_BOLD + theResumeVerbose + ESC_RESET + "\" (excluding the surrounding quotes)..." );
			}
		}
	}

	private static AlphabetCounterMetrics toAlphabetCounterMetrics( String aStartValue, int aMinLength, int aMaxLength, String aAlphabet, CharSet theCharSet ) {
		if ( aAlphabet == null && theCharSet == null ) {
			theCharSet = DEFAULT_CHARSET;
		}
		List<Character> theChars = new ArrayList<>();
		if ( aAlphabet != null && aAlphabet.length() != 0 ) {
			for ( int i = 0; i < aAlphabet.length(); i++ ) {
				if ( !theChars.contains( aAlphabet.charAt( i ) ) ) {
					theChars.add( aAlphabet.charAt( i ) );
				}
			}
		}
		if ( theCharSet != null ) {
			for ( char eChar : theCharSet.getCharSet() ) {
				if ( !theChars.contains( eChar ) ) {
					theChars.add( eChar );
				}
			}
		}
		if ( theChars.isEmpty() ) {
			throw new IllegalStateException( "Either the <" + ALPHABET_PROPERTY + "> or the <" + CHARSET_PROPERTY + "> arguments (properties) must be set!" );
		}
		Collections.sort( theChars );
		char[] theAlphabet = new char[theChars.size()];
		for ( int i = 0; i < theAlphabet.length; i++ ) {
			theAlphabet[i] = theChars.get( i );
		}
		return new AlphabetCounterMetrics( aStartValue, aMinLength, aMaxLength, theAlphabet );
	}

	private static File toProjectFile( String aProjectFilePath, boolean isVerbose ) {
		File theProjectFile = new File( aProjectFilePath );
		if ( isVerbose ) {
			LOGGER.info( "Project file = \"" + theProjectFile.getAbsolutePath() + "\"" );
		}
		return theProjectFile;
	}

	private static File toZipFile( String aInFilePath, boolean isVerbose ) throws FileNotFoundException {
		File theZipFile = new File( aInFilePath );
		if ( isVerbose ) {
			LOGGER.info( "ZIP file = \"" + theZipFile.getAbsolutePath() + "\"" );
		}
		if ( !theZipFile.exists() || !theZipFile.isFile() ) {
			throw new FileNotFoundException( "No input file \"" + aInFilePath + "\" (<" + theZipFile.getAbsolutePath() + ">) found!" );
		}
		return theZipFile;
	}

	private static void init( String aProjectFilePath, boolean isVerbose ) {
		File theProjectFile = toProjectFile( aProjectFilePath, false );
		if ( theProjectFile.isFile() && theProjectFile.delete() ) {
			if ( isVerbose ) {
				LOGGER.info( "Deleted project file \"" + aProjectFilePath + "\" (<" + theProjectFile.getAbsolutePath() + ">)." );
			}
		}
		else {
			if ( isVerbose ) {
				LOGGER.warn( "No such project file \"" + aProjectFilePath + "\" (<" + theProjectFile.getAbsolutePath() + ">), aborting!" );
			}
			else {
				System.err.println( "No such project file \"" + aProjectFilePath + "\" (<" + theProjectFile.getAbsolutePath() + ">), aborting!" );
			}
			System.exit( ExitCode.NOT_FOUND.getStatusCode() );
		}
	}

	private static void info( String aZipFilePath, boolean isVerbose ) throws IOException {
		try ( ZipFile theZipFile = new ZipFile( toZipFile( aZipFilePath, isVerbose ) ) ) {
			boolean isValidZip = theZipFile.isValidZipFile();
			boolean isEncrypted = theZipFile.isEncrypted();
			boolean isSplitArchive = theZipFile.isSplitArchive();
			String theComment = theZipFile.getComment();
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "Is valid ZIP (*.zip) file = " + isValidZip );
				LOGGER.info( "Is encrypted = " + isEncrypted );
				LOGGER.info( "Is split archive = " + isSplitArchive );
				LOGGER.info( "ZIP (*.zip) file comment = \"" + theComment + "\"" );
			}
			else {
				System.out.println( "Is valid ZIP (*.zip) file = " + isValidZip );
				System.out.println( "Is encrypted = " + isEncrypted );
				System.out.println( "Is split archive = " + isSplitArchive );
				System.out.println( "ZIP (*.zip) file comment = \"" + theComment + "\"" );
			}
			List<FileHeader> theHeaders = theZipFile.getFileHeaders();
			if ( isVerbose ) {
				LOGGER.printSeparator();
			}
			for ( FileHeader eHeader : theHeaders ) {
				if ( isVerbose ) {
					LOGGER.info( eHeader.getFileName() + " (" + eHeader.getUncompressedSize() + " bytes)" );
				}
				else {
					System.out.println( eHeader.getFileName() + " (" + eHeader.getUncompressedSize() + " bytes)" );
				}
			}
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "ZIP (*.zip) file contains a total of <" + theHeaders.size() + "> files." );
			}
			else {
				System.out.println( "ZIP (*.zip) file contains a total of <" + theHeaders.size() + "> files." );
			}
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "Available hardware threads = " + Runtime.getRuntime().availableProcessors() );
			}
			else {
				System.out.println( "Available hardware threads = " + Runtime.getRuntime().availableProcessors() );
			}
		}
	}
}
