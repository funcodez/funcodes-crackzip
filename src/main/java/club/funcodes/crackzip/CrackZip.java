// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.crackzip;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;

import org.refcodes.component.StopException;
import org.refcodes.component.Stoppable.StopAutomaton;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.exception.ExceptionAccessor;
import org.refcodes.generator.AlphabetCounter;
import org.refcodes.generator.BufferedGenerator;
import org.refcodes.generator.IdCounter;
import org.refcodes.generator.ThreadLocalBufferedGeneratorDecorator;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.inputstream.ZipInputStream;
import net.lingala.zip4j.model.LocalFileHeader;

/**
 * The {@link CrackZip} tries to humbly crack a ZIP file's password.
 */
public class CrackZip implements StopAutomaton, ExceptionAccessor<Exception> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long GC_INTERVALL_MS = 1000 * 60 * 60; // 1h

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _priority;
	private final Thread[] _threads;
	private final CrackZipFile[] _runnables;
	private final BufferedGenerator<String> _passwordCounter;
	private final AtomicLong _counter = new AtomicLong( 0 );
	private final Pattern _yesRegex;
	private final Pattern _noRegex;
	private Exception _exception = null;
	private String _password = null;
	private String _lastProcessed = null;
	private boolean _isRunning = false;
	private static long _lastGarbageCollectionTime = System.currentTimeMillis();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs and starts the {@link ZipFile} cracker.
	 *
	 * @param aZipFile The {@link File} pointing to the ZIP-File to process.
	 * @param aOutputDir The folder where to write the content of the cracked
	 *        archive to.
	 * @param aAlphabetCounter The {@link AlphabetCounter} to use when creating
	 *        the passwords.
	 * @param aYesPattern A regular expression which must match a password to be
	 *        included(!) for probing.
	 * @param aNoPattern A regular expression which must match a password to be
	 *        skipped(!) for probing.
	 * @param aResume The password from which to start (resume).
	 * @param aMinLength The min length of the passwords to probe.
	 * @param aMaxLength The max length of the passwords to probe.
	 * @param aThreads number of threads to use.
	 * @param aPriority The priority for the threads to use.
	 * @param isNoCache Experimental! Do not load the content of the ZIP file
	 *        into memory prior to cracking (may result in an "out of file
	 *        handles" exception!)
	 * 
	 * @throws IOException thrown in case in I/O related problem occurred
	 *         operating on the {@link ZipFile}.
	 */
	public CrackZip( File aZipFile, File aOutputDir, IdCounter aAlphabetCounter, Pattern aYesPattern, Pattern aNoPattern, int aThreads, int aPriority, boolean isNoCache ) throws IOException {
		_threads = new Thread[aThreads];
		_runnables = new CrackZipFile[aThreads];
		_priority = aPriority;
		_yesRegex = aYesPattern;
		_noRegex = aNoPattern;
		try ( ZipFile theZipFile = new ZipFile( aZipFile ) ) {
			if ( !theZipFile.isEncrypted() ) {
				throw new IllegalArgumentException( "Noting to do as the provided file <" + aZipFile.getAbsolutePath() + "> is not encrypted!" );
			}
		}
		_passwordCounter = new ThreadLocalBufferedGeneratorDecorator<String>( aAlphabetCounter );
		// _passwordCounter = new ConcurrentBufferedGeneratorDecorator<String>( aAlphabetCounter );
		ByteArrayOutputStream theZipBufferStream = null;
		if ( !isNoCache ) {
			theZipBufferStream = new ByteArrayOutputStream( (int) aZipFile.length() );
			try ( InputStream theInStream = new FileInputStream( aZipFile ) ) {
				theInStream.transferTo( theZipBufferStream );
			}
		}
		_isRunning = true;
		for ( int i = 0; i < _threads.length; i++ ) {
			_runnables[i] = new CrackZipFile( aZipFile, theZipBufferStream != null ? new ByteArrayInputStream( theZipBufferStream.toByteArray() ) : null, aOutputDir );
			_threads[i] = new Thread( _runnables[i] );
			_threads[i].setDaemon( false );
			_threads[i].setPriority( _priority );
			_threads[i].start();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the number of passwords tried out.
	 * 
	 * @return The according number of probed passwords.
	 */
	public long getProbedPasswordsCount() {
		return _counter.get();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Exception getException() {
		return _exception;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Stops all running threads and waits for them to finish.
	 */
	@Override
	public synchronized void stop() throws StopException {
		if ( _isRunning ) {
			_isRunning = false;
			_passwordCounter.suspend();
			ControlFlowUtility.joinThreads( _threads );
		}
		else {
			throw new StopException( "This instance has not being started prior to stopping it!" );
		}
	}

	/**
	 * Determines the password from which to resume from after abortion. This
	 * method can only be called upon being stopped as of {@link #stop()}.
	 * 
	 * @return The next password from which to resume.
	 */
	//	public String toResumeFrom() {
	//		return _passwordCounter.getDecoratee().hasNext() ? _passwordCounter.getDecoratee().next() : null;
	//	}

	/**
	 * Returns the password identified by the started {@link CrackZip} instance.
	 * 
	 * @return The password or null if none was found (yet).
	 */
	public String getPassword() {
		return _password;
	}

	/**
	 * Returns the last password from which resuming is possible without
	 * skipping password.
	 * 
	 * @return The last password from which to resume.
	 */
	public String lastProcessed() {
		return _lastProcessed;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStoppable() {
		return _isRunning;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStopped() {
		for ( CrackZipFile _runnable : _runnables ) {
			if ( _runnable.isRunning() ) {
				return false;
			}
		}
		return true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	class CrackZipFile implements Runnable {

		private final File _zipFile;
		private final InputStream _zipInStream;
		private final File _outputDir;
		private boolean _isRunning = false;

		CrackZipFile( File aZipFile, InputStream aZipInStream, File aOutputDir ) {
			_zipFile = aZipFile;
			_zipInStream = aZipInStream;
			_outputDir = aOutputDir;
			if ( _zipInStream != null ) {
				_zipInStream.mark( (int) aZipFile.length() );
			}
		}

		@Override
		public void run() {
			_isRunning = true;
			String eNext;
			InputStream eInStream = _zipInStream;
			LocalFileHeader eNextEntry;
			// Matcher theYesMatcher = null;
			// Matcher theNoMatcher = null;
			// if ( _yesRegex != null ) {
			//	theYesMatcher = _yesRegex.matcher( "" );
			// }
			// if ( _noRegex != null ) {
			//	theNoMatcher = _noRegex.matcher( "" );
			// }
			try {
				while ( _passwordCounter.hasNext() ) {
					eNext = _passwordCounter.next();
					_counter.incrementAndGet();
					_lastProcessed = eNext;
					// if ( theYesMatcher != null && !theYesMatcher.reset( eNext ).matches() ) {
					//	continue;
					// }
					// if ( theNoMatcher != null && theNoMatcher.reset( eNext ).matches() ) {
					//	continue;
					// }
					if ( _yesRegex != null && !_yesRegex.matcher( eNext ).matches() ) {
						continue;
					}
					if ( _noRegex != null && _noRegex.matcher( eNext ).matches() ) {
						continue;
					}
					if ( _zipInStream != null ) {
						_zipInStream.reset();
					}
					else {
						eInStream = new FileInputStream( _zipFile );
					}

					try ( ZipInputStream eZipInStream = new ZipInputStream( eInStream ) ) {
						eZipInStream.setPassword( eNext.toCharArray() );
						eNextEntry = toFileHeader( eZipInStream );
						if ( eNextEntry != null ) {
							for ( int i = 0; i < eNextEntry.getUncompressedSize(); i++ ) {
								while ( eZipInStream.read() != -1 ) {} // Test CRC
							}
							_password = eNext;
							CrackZip.this._isRunning = false;
							CrackZip.this._passwordCounter.suspend();
							if ( _outputDir != null ) {
								try ( ZipFile theZipFile = new ZipFile( _zipFile ) ) {
									theZipFile.setPassword( eNext.toCharArray() );
									theZipFile.extractAll( _outputDir.getAbsolutePath() );
								}
							}
							synchronized ( CrackZip.this ) {
								CrackZip.this.notifyAll();
							}
						}
					}
					catch ( ZipException ignore ) {}
					catch ( IOException e ) {
						if ( e.getCause() == null || !( e.getCause() instanceof DataFormatException ) ) {
							throw e;
						}
					}

					if ( _lastGarbageCollectionTime + GC_INTERVALL_MS < System.currentTimeMillis() ) {
						synchronized ( CrackZipFile.class ) {
							if ( _lastGarbageCollectionTime + GC_INTERVALL_MS < System.currentTimeMillis() ) {
								if ( LOGGER.isLogInfo() ) {
									LOGGER.printSeparator();
									LOGGER.info( "Executing garbage collection (GC)..." );
									LOGGER.printSeparator();
								}
								System.gc();
								_lastGarbageCollectionTime = System.currentTimeMillis();
							}
						}
					}
				}
			}
			catch ( IOException e ) {
				CrackZip.this._exception = e;
				CrackZip.this._isRunning = false;
				CrackZip.this._passwordCounter.suspend();
			}
			finally {
				_isRunning = false;
			}
		}

		/**
		 * Determines whether this {@link Runnable} is running.
		 * 
		 * @return True in case of running.
		 */
		public boolean isRunning() {
			return _isRunning;
		}
	}

	private static LocalFileHeader toFileHeader( ZipInputStream eZipInStream ) throws IOException {
		LocalFileHeader eNextEntry = eZipInStream.getNextEntry();
		while ( eNextEntry != null && ( eNextEntry.isDirectory() || eNextEntry.getUncompressedSize() == 0 ) ) {
			eNextEntry = eZipInStream.getNextEntry();
		}
		return eNextEntry;
	}

}
